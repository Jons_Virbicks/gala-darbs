<?php

session_start();

if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: index.php");
  exit;
}

?>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Drift parts</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="../style.css" />
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
        
    </head>
    <body>
        <header class="container-fluid">
            <div class="row">
                <a href="../index.php">
                    <img src="../img/logo1.png" style="width:50px; height:50px;" >
                </a>
                <nav class="col-sm-10">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link active" href="../index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../shop.php">Shop</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../about.php">About us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../contacts.php">Contacts</a>
                        </li>
                    </ul>
                </nav>
                <a class="btn btn-primary login" href="index.php" role="button">Log in</a>
            </div>
        </header>
        <main>
            <div class="container">
                <div class="page-header">
                    <h2>Hi, <b><?php echo htmlspecialchars($_SESSION['username']); ?></b>. Welcome.</h2>
                </div>
                <p><a href="logout.php" class="btn btn-danger">Logout</a></p>
                <div class="row">
                    <div class="col-sm-4 offset-sm-4">
                        <form action="upload.php" method="post" enctype="multipart/form-data">
                            <input type="text" name="product_title" class="form-control" placeholder="Enter product name">
                            <input type="text" name="product_price" class="form-control" placeholder="Enter price">
                            <input type="file" name="fileToUpload" id="fileToUpload">
                            <input type="submit"  class="btn btn-primary" value="Upload" name="submit">
                        </form>
                    </div>
                </div>
                <div class="row imgProd">
                    <div class="col-sm-4 offset-sm-2">
                        <a href="#">
                            <img src="../img/hidro.jpg">
                        </a>
                        <div class="text-block">
                            <h4>99$</h4>
                        </div>
                        <h2>Hydraulic handbrake</h2>
                        <p>Discription</p>
                    </div>
                    <div class="col-sm-4">
                        <a href="#">
                            <img src="../img/wisefab.jpg">
                        </a>
                        <div class="text-block">
                            <h4>1050$</h4>
                        </div>
                        <h2>Wisefab angle kit</h2>
                        <p>Discription</p>
                    </div>
                </div>
            </div>
        </main>
        <footer class="container-fluid">
            <a href="../index.php" class="col-sm-1">
                <img src="../img/logo1.png" style="width:70px; height:70px;" >
            </a>
            <nav class="col-sm-11">
                <ul class="nav justify-content-end">
                    <li class="nav-item">
                        <a class="nav-link active" href="../index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../shop.php">Shop</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../about.php">About us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../contacts.php">Contacts</a>
                    </li>
                </ul>
            </nav>
            <h4>Copyright text</h4>
        </footer>
    </body>
</html>
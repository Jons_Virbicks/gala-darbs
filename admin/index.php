<!DOCTYPE html>
<?php
session_start();

require_once 'config.php';

$username = $password = "";
$username_err = $password_err = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){

    if(empty(trim($_POST["username"]))){
        $username_err = 'Please enter username.';
    } else{
        $username = trim($_POST["username"]);
    }

    if(empty(trim($_POST['password']))){
        $password_err = 'Please enter your password.';
    } else{
        $password = trim($_POST['password']);
    }

    if(empty($username_err) && empty($password_err)){
        $sql = "SELECT username, password FROM users WHERE username = ?";

        if($stmt = mysqli_prepare($link, $sql)){
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            $param_username = $username;

            if(mysqli_stmt_execute($stmt)){
                mysqli_stmt_store_result($stmt);
                if(mysqli_stmt_num_rows($stmt) == 1){                    
                    mysqli_stmt_bind_result($stmt, $username, $hashed_password);

                    if(mysqli_stmt_fetch($stmt)){
                        if(password_verify($password, $hashed_password)){
                            session_start();
                            $_SESSION['username'] = $username;      
                            header("location: editor.php");
                        } else{
                            $password_err = 'The password you entered was not valid.';
                        }
                    }
                } else{
                    $username_err = 'No account found with that username.';
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

        }
        mysqli_stmt_close($stmt);

    }
    mysqli_close($link);
}

?>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Drift parts</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="../style.css" />
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
        
    </head>
    <body>
        <header class="container-fluid">
            <div class="row">
                <a href="../index.php">
                    <img src="../img/logo1.png" style="width:50px; height:50px;" >
                </a>
                <nav class="col-sm-10">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link active" href="../index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../shop.php">Shop</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../about.php">About us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../contacts.php">Contacts</a>
                        </li>
                    </ul>
                </nav>
                <a class="btn btn-primary login" href="index.php" role="button">Log in</a>
            </div>
        </header>
        <main>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 offset-sm-3">
                        <h2>Admin login</h2>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        <form action="index.php" method="post">
                            <input type="text" name="username" class="form-control" placeholder="Enter email">
                            <input type="password" name="password" class="form-control" placeholder="Enter password">
                            <button class="btn btn-primary" name="submit" type="submit">login</button>
                        </form>
                    </div>
                </div>
            </div>
        </main>
        <footer class="container-fluid">
            <a href="../index.php" class="col-sm-1">
                <img src="../img/logo1.png" style="width:70px; height:70px;" >
            </a>
            <nav class="col-sm-11">
                <ul class="nav justify-content-end">
                    <li class="nav-item">
                        <a class="nav-link active" href="../index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../shop.php">Shop</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../about.php">About us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../contacts.php">Contacts</a>
                    </li>
                </ul>
            </nav>
            <h4>Copyright text</h4>
        </footer>
    </body>
</html>
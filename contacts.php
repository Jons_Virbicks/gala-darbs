<?php require("layout/header.php");
 
if(isset($_POST['submit'])){
    $to = "jon4ix@inbox.lv";
    $from = $_POST['email'];
    $first_name = $_POST['name'];
    $subject = "Form submission";
    $message = $first_name . " " . " wrote the following:" . "\n\n" . $_POST['message'];

    $headers = "From:" . $from;
    mail($to,$subject,$message,$headers);
    echo "Mail Sent. Thank you " . $first_name;
    }
?>
<div class="container">
    <h2>Headline</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce placerat, elit sed consequat luctus, magna eros lobortis neque, ut porttitor libero risus id urna. Duis mattis consectetur risus ut eleifend. Nam dapibus aliquam erat, ac ornare eros aliquet vel. Nulla facilisi. Maecenas rutrum pellentesque tempus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse eu bibendum dui, ac aliquet felis. Nunc elementum purus vulputate pharetra ultrices. Nam in vestibulum nisi. 
    </p>
    <div class="row">
        <div class="col-sm-6">
        <form action="" method="post">
            <div class="form-group">
                <input type="name" name="name" class="form-control" placeholder="Enter name">
                <input type="email" name="email" class="form-control" placeholder="Enter email">
                <textarea class="form-control" name="message" id="exampleFormControlTextarea1" rows="4" placeholder="Enter message"></textarea>
                <button class="btn btn-primary" name="submit" type="submit">Send</button>
            </div>
        </form>
        </div>
        <div class="col-sm-6">
            <div style="width: 100%">
                <iframe width="100%" height="300" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=Riga%2C%20B%C4%81kas%20iela+(My%20Business%20Name)&amp;ie=UTF8&amp;t=&amp;z=17&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                <a href="https://www.maps.ie/create-google-map/">Embed Google Map</a>
                </iframe>
            </div>
        </div>
    </div>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce placerat, elit sed consequat luctus, magna eros lobortis neque, ut porttitor libero risus id urna. Duis mattis consectetur risus ut eleifend. Nam dapibus aliquam erat, ac ornare eros aliquet vel. Nulla facilisi. Maecenas rutrum pellentesque tempus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse eu bibendum dui, ac aliquet felis. Nunc elementum purus vulputate pharetra ultrices. Nam in vestibulum nisi. 
    </p>
</div>
<?php require("layout/footer.php");?>

<?php require("layout/header.php");?>
<main class="container">
    <div class="row">
        <div class="col-sm-4 offset-sm-4">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li href="img/hsd.jpg" data-slide-to="0" class="active"></li>
                    <li href="img/rota.jpg" data-slide-to="1"></li>
                    <li href="img/wisefab.jpg" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="img/hsd.jpg" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="img/rota.jpg" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="img/wisefab.jpg" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>  
    <div>
        <h1>Teksts teksts teksts</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce placerat, elit sed consequat luctus, magna eros lobortis neque, ut porttitor libero risus id urna. Duis mattis consectetur risus ut eleifend.</p>
        <div class="row imgProd">
            <div class="col-sm-4 offset-sm-2">
                <a href="#">
                    <img src="img/hsd.jpg">
                </a>
                <div class="text-block">
                    <h4>799$</h4>
                </div>
                <h2>Hsd coilovers</h2>
                <p>Discription</p>
            </div>
            <div class="col-sm-4">
                <a href="#">
                    <img src="img/rota.jpg">
                </a>
                <div class="text-block">
                    <h4>200$</h4>
                </div>
                <h2>Rota wheels</h2>
                <p>Discription</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2 offset-sm-5">
            <a class="btn btn-primary" href="shop.php" role="button">More products</a>
        </div>
    </div>
</main>
<?php require("layout/footer.php");?>

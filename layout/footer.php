        <footer class="container-fluid">
            <a href="index.php" class="col-sm-1">
                <img src="img/logo1.png" style="width:70px; height:70px;" >
            </a>
            <nav class="col-sm-11">
                <ul class="nav justify-content-end">
                    <li class="nav-item">
                        <a class="nav-link active" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="shop.php">Shop</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.php">About us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contacts.php">Contacts</a>
                    </li>
                </ul>
            </nav>
            <h4>Copyright text</h4>
        </footer>
    </body>
</html>
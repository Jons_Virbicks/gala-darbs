<?php require("layout/header.php");?>
<div class="container imgProd">
    <div class="row">
        <div class="col-sm-4 offset-sm-2">
            <a href="#">
                <img src="img/hsd.jpg">
            </a>
            <div class="text-block">
                <h4>799$</h4>
            </div>
            <h2>Hsd coilovers</h2>
            <p>Discription</p>
        </div>
        <div class="col-sm-4">
            <a href="#">
                <img src="img/rota.jpg">
            </a>
            <div class="text-block">
                <h4>200$</h4>
            </div>
            <h2>Rota wheels</h2>
            <p>Discription</p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 offset-sm-2">
            <a href="#">
                <img src="img/hidro.jpg">
            </a>
            <div class="text-block">
                <h4>99$</h4>
            </div>
            <h2>Hydraulic handbrake</h2>
            <p>Discription</p>
        </div>
        <div class="col-sm-4">
            <a href="#">
                <img src="img/wisefab.jpg">
            </a>
            <div class="text-block">
                <h4>1050$</h4>
            </div>
            <h2>Wisefab angle kit</h2>
            <p>Discription</p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 offset-sm-4">
            <a class="btn btn-primary" href="shop.php" role="button">1</a>
            <a class="btn btn-primary" href="shop2.php" role="button">2</a>
            <a class="btn btn-primary" href="#" role="button">3</a>
            <a class="btn btn-primary" href="#" role="button">4</a>
        </div>
    </div>
</div>
<?php Require("layout/footer.php");?>